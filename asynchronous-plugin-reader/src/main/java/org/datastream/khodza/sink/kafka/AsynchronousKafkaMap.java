package org.datastream.khodza.sink.kafka;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.connectors.influxdb.InfluxDBPoint;
import org.datastream.khodza.model.avro.t_scele_public_mdl_asynchronous.AsynchronousValue;
import org.datastream.khodza.model.avro.t_scele_public_mdl_logstore_standard_log.LogValue;

import java.lang.reflect.Type;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;

public class AsynchronousKafkaMap extends RichMapFunction<AsynchronousValue, String> {

    String namespace;

    public AsynchronousKafkaMap(String namespace) {
        super();
        this.namespace = namespace;
    }

    @Override
    public String map (AsynchronousValue s) {

        long timestamp = s.getTimestamp() * 1000;
        HashMap<String, Object> map = new HashMap<>();

        map.put("id", s.getId());
        map.put("attempt_id", s.getAttemptid());
        map.put("cm_id", s.getCmid());
        map.put("course_id", s.getCourseid());
        map.put("user_id", s.getUserid());
        map.put("activity", s.getActivity());
        map.put("activity_code", s.getActivitycode());
        map.put("timestamp", s.getTimestamp());

        map.put("load_time", Instant.now().toEpochMilli());

        map.forEach((k, v) -> map.put(k, v == null ? "" : v.toString()));

        Gson gson = new Gson();
        Type gsonType = new TypeToken<HashMap>(){}.getType();
        String json = gson.toJson(map,gsonType);

        return json;
    };

}
