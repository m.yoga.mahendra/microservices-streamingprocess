package org.datastream.khodza.sink.influx;

import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.streaming.connectors.influxdb.InfluxDBPoint;
import org.datastream.khodza.model.avro.t_scele_public_mdl_asynchronous.AsynchronousValue;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;

public class AsynchronousInfluxMap extends RichMapFunction<AsynchronousValue, InfluxDBPoint> {

    String namespace;

    public AsynchronousInfluxMap(String namespace) {
            super();
            this.namespace = namespace;
    }

    @Override
    public InfluxDBPoint map(
            AsynchronousValue s) throws Exception {

        String measurement = "t-scele-asynchronous";
        long timestamp = s.getTimestamp() * 1000;

        HashMap<String, String> tags = new HashMap<>();
        HashMap<String, Object> fields = new HashMap<>();
        fields.put("id", s.getId());
        fields.put("attempt_id", s.getAttemptid());
        fields.put("cm_id", s.getCmid());
        fields.put("course_id", s.getCourseid());
        fields.put("user_id", s.getUserid());

        fields.put("activity", s.getActivity());
        fields.put("activity_code", s.getActivitycode());
        fields.put("timestamp", s.getTimestamp());

        fields.forEach((k, v) -> fields.put(k, v == null ? "" : v.toString()));
        Iterables.removeIf(tags.values(), Predicates.isNull());
        Iterables.removeIf(fields.values(), Predicates.isNull());

        //        Time offset metric
        Duration timeElapsed = Duration.between(Instant.ofEpochMilli(timestamp), Instant.now());
        fields.put("latency", timeElapsed.toMillis());

        URL url = new URL("http://192.168.43.220:8086/write?db=t_integration_learninganalytics");
        HttpURLConnection http = (HttpURLConnection)url.openConnection();
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

        String data = measurement + " ";
        for (Object key: fields.keySet()) {
            data += key + "=" + "\"" + fields.get(key) + "\"" + ",";
        }
        data = data.substring(0, data.length() - 1);

        byte[] out = data.getBytes(StandardCharsets.UTF_8);

        OutputStream stream = http.getOutputStream();
        stream.write(out);

        System.out.println(http.getResponseCode() + " " + http.getResponseMessage());
        http.disconnect();

        return new InfluxDBPoint(measurement, timestamp, tags, fields);
    }
}