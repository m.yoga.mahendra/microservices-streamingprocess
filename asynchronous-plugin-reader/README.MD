# Khodza: Real Time Stream Aggregator
The use-cases of real time data analytics  needs a real-time data processing service. Khodza is a real time stream aggregator built on top of Apache Flink. 
Currently Khodza accepts these data sources:
- Apache Kafka
- Elasticsearch 6+

Khodza sinks its aggregation results into these outputs:
- Apache Kafka
- InfluxDB
- Elasticsearch 6+

To compile:
```$xslt
mvn clean package
```

To run:
```
<target execution class> <target job properties>
```

for example:

```
org.datastream.khodza.job.Scele scele.s1.properties
```

CREATE DATABASE t_integration_learninganalytics

curl -G 'http://10.128.0.2:8086/query?pretty=true' --data-urlencode "db=t_integration_learninganalytics" --data-urlencode "q=SELECT * FROM \"cpu_load_short\""

curl -i -XPOST 'http://localhost:8086/write?db=t_integration_learninganalytics' --data-binary 'cpu_load_short,host=server01,region=us-west value=0.64 1434055562000000000'
curl -i -XPOST 'http://localhost:8086/write?db=t_integration_learninganalytics' --data-binary 't-scele-asynchronous,id=201,attemptid=58,cmid=2,courseid=2,userid=2,activitycode=1,activity=slot:1;qno:2;answer:a.2a,timestamp=1630145703288 value=0.54'
curl -i -XPOST 'http://localhost:8086/write?db=t_integration_learninganalytics' --data-binary 't-scele-asynchronous,id=201,attemptid=58,cmid=2,courseid=2,userid=2,activitycode=1,timestamp=1630145703288 value=0.54'
